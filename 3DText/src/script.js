import './style.css'
import * as THREE from 'three'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js'
import * as dat from 'dat.gui'

/**
 * Base
 */
// Debug
const gui = new dat.GUI()

// Canvas
const canvas = document.querySelector('canvas.webgl')

// Scene
const scene = new THREE.Scene()

/**
 * Textures
 */
const textureLoader = new THREE.TextureLoader()

/**
 * Axes Helper
 */

// const axesHelper = new THREE.AxesHelper()
// scene.add(axesHelper)

/**
 * Fonts
 */

const fontLoader = new THREE.FontLoader();
fontLoader.load(
    // resource URL
    'fonts/helvetiker_regular.typeface.json',

    // onLoad callback
    function (font) {
        const textGeometry = new THREE.TextBufferGeometry(
            'Hello Three JS',
            {
                font,
                size: 0.5,
                height: 0.2,
                curveSegments: 6,
                bevelEnabled: true,
                bevelThickness: 0.03,
                bevelSegments: 4,
                bevelSize: 0.02,
                bevelOffset: 0
            }
        )

        // textGeometry.computeBoundingBox()
        // textGeometry.translate(
        //     - textGeometry.boundingBox.max.x * 0.5,
        //     - textGeometry.boundingBox.max.y * 0.5,
        //     - textGeometry.boundingBox.max.z * 0.5 
        // )

        textGeometry.center()

        const texture = textureLoader.load('/textures/matcaps/8.png')
        const material = new THREE.MeshMatcapMaterial()
        material.matcap = texture

        const text = new THREE.Mesh(textGeometry, material)
        scene.add(text)

        console.time('Donuts');

        const donutGeometry = new THREE.TorusBufferGeometry(0.3, 0.2, 20, 45)

        for (let i = 0; i < 100; i++) {
            const donut = new THREE.Mesh(donutGeometry, material)

            donut.position.set(
                (Math.random() - 0.5) * 10,
                (Math.random() - 0.5) * 10,
                (Math.random() - 0.5) * 10
            )

            donut.rotation.set(
                Math.random() * Math.PI,
                Math.random() * Math.PI,
                0
            )

            const scaleRandom = Math.random()
            donut.scale.set(scaleRandom, scaleRandom, scaleRandom)

            scene.add(donut)
        }

        console.timeEnd('Donuts');
    },
    function (xhr) {
        console.log((xhr.loaded / xhr.total * 100) + '% loaded');
    },
    function (err) {
        console.log('An error happened');
    }
);

/**
 * Sizes
 */
const sizes = {
    width: window.innerWidth,
    height: window.innerHeight
}

window.addEventListener('resize', () => {
    // Update sizes
    sizes.width = window.innerWidth
    sizes.height = window.innerHeight

    // Update camera
    camera.aspect = sizes.width / sizes.height
    camera.updateProjectionMatrix()

    // Update renderer
    renderer.setSize(sizes.width, sizes.height)
    renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))
})

/**
 * Camera
 */
// Base camera
const camera = new THREE.PerspectiveCamera(75, sizes.width / sizes.height, 0.1, 100)
camera.position.x = 1
camera.position.y = 1
camera.position.z = 2
scene.add(camera)

// Controls
const controls = new OrbitControls(camera, canvas)
controls.enableDamping = true

/**
 * Renderer
 */
const renderer = new THREE.WebGLRenderer({
    canvas: canvas
})
renderer.setSize(sizes.width, sizes.height)
renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))

/**
 * Animate
 */
const clock = new THREE.Clock()

const tick = () => {
    const elapsedTime = clock.getElapsedTime()

    // Update controls
    controls.update()

    // Render
    renderer.render(scene, camera)

    // Call tick again on the next frame
    window.requestAnimationFrame(tick)
}

tick()