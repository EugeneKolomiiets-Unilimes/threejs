import "./style.css";
import * as three from "three";
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls.js";
import * as dat from "dat.gui";

/**
 * Base
 */
// Debug
const gui = new dat.GUI();

// Canvas
const canvas = document.querySelector("canvas.webgl");

// Scene
const scene = new three.Scene();

// Fog
const fog = new three.Fog('#262836', 1, 15)
scene.fog = fog
/**
 * Textures
 */
const textureLoader = new three.TextureLoader();

const doorColorTexture = textureLoader.load('/textures/door/color.jpg')
const doorAlphaTexture = textureLoader.load('/textures/door/alpha.jpg')
const doorAmbientTexture = textureLoader.load('/textures/door/ambientOcclusion.jpg')
const doorHeightTexture = textureLoader.load('/textures/door/height.jpg')
const doorNormalTexture = textureLoader.load('/textures/door/normal.jpg')
const doorMetalnessTexture = textureLoader.load('/textures/door/metalness.jpg')
const doorRoughnessTexture = textureLoader.load('/textures/door/roughness.jpg')

const bricksColorTexture = textureLoader.load('/textures/bricks/color.jpg')
const bricksAmbientTexture = textureLoader.load('/textures/bricks/ambientOcclusion.jpg')
const bricksNormalTexture = textureLoader.load('/textures/bricks/normal.jpg')
const bricksRoughnessTexture = textureLoader.load('/textures/bricks/roughness.jpg')

const grassColorTexture = textureLoader.load('/textures/grass/color.jpg')
const grassAmbientTexture = textureLoader.load('/textures/grass/ambientOcclusion.jpg')
const grassNormalTexture = textureLoader.load('/textures/grass/normal.jpg')
const grassRoughnessTexture = textureLoader.load('/textures/grass/roughness.jpg')

grassColorTexture.repeat.set(8, 8)
grassAmbientTexture.repeat.set(8, 8)
grassNormalTexture.repeat.set(8, 8)
grassRoughnessTexture.repeat.set(8, 8)

grassColorTexture.wrapS = three.RepeatWrapping
grassAmbientTexture.wrapS = three.RepeatWrapping
grassNormalTexture.wrapS = three.RepeatWrapping
grassRoughnessTexture.wrapS = three.RepeatWrapping

grassColorTexture.wrapT = three.RepeatWrapping
grassAmbientTexture.wrapT = three.RepeatWrapping
grassNormalTexture.wrapT = three.RepeatWrapping
grassRoughnessTexture.wrapT = three.RepeatWrapping
/**
 * House
 */

// Group

const house = new three.Group();
scene.add(house);

// Walls
const walls = new three.Mesh(
    new three.BoxBufferGeometry(4, 2.5, 4),
    new three.MeshStandardMaterial({
        map: bricksColorTexture,
        aoMap: bricksAmbientTexture,
        normalMap: bricksNormalTexture,
        roughnessMap: bricksRoughnessTexture,
    })
)
walls.position.y = 1.25
walls.geometry.setAttribute('uv2', new three.Float32BufferAttribute(walls.geometry.attributes.uv.array, 2))
house.add(walls)

// Roof
const roof = new three.Mesh(
    new three.ConeBufferGeometry(3.5, 1, 4),
    new three.MeshStandardMaterial({ color: "#b35f45" })
)
roof.position.y = 2.5 + 0.5
roof.rotation.y = Math.PI * 0.25
house.add(roof)

// Door
const door = new three.Mesh(
    new three.PlaneBufferGeometry(2.2, 2.2, 100, 100),
    new three.MeshStandardMaterial({
        map: doorColorTexture,
        alphaMap: doorAlphaTexture,
        transparent: true,
        aoMap: doorAmbientTexture,
        displacementMap: doorHeightTexture,
        displacementScale: 0.1,
        normalMap: doorNormalTexture,
        metalnessMap: doorMetalnessTexture,
        roughnessMap: doorRoughnessTexture,
    })
)
door.geometry.setAttribute('uv2', new three.Float32BufferAttribute(door.geometry.attributes.uv.array, 2))
door.position.z = 2.01
door.position.y = 1
house.add(door)

// Bushes
const bushGeometry = new three.SphereBufferGeometry(1, 16, 16)
const bushMaterial = new three.MeshStandardMaterial({ color: '#89c854' })

const bushOne = new three.Mesh(bushGeometry, bushMaterial)
bushOne.scale.set(0.5, 0.5, 0.5)
bushOne.position.set(1.2, 0.2, 2.6)
const bushTwo = new three.Mesh(bushGeometry, bushMaterial)
bushTwo.scale.set(0.25, 0.25, 0.25)
bushTwo.position.set(1.8, 0.2, 2.4)
const bushThree = new three.Mesh(bushGeometry, bushMaterial)
bushThree.scale.set(0.3, 0.3, 0.3)
bushThree.position.set(-2.2, 0.2, 2.3)
const bushFour = new three.Mesh(bushGeometry, bushMaterial)
bushFour.scale.set(0.4, 0.4, 0.4)
bushFour.position.set(-1.7, 0.2, 2.6)
house.add(bushOne, bushTwo, bushThree, bushFour)

//Graves
const graves = new three.Group()
scene.add(graves)

const graveGeometry = new three.BoxBufferGeometry(0.6, 0.8, 0.2)
const graveMaterial = new three.MeshStandardMaterial({ color: '#b2b6b1' })

for (let i = 0; i < 50; i++) {
    const angle = Math.random() * Math.PI * 2;
    const radius = 3 + Math.random() * 6;
    const x = Math.sin(angle) * radius;
    const z = Math.cos(angle) * radius;

    const grave = new three.Mesh(graveGeometry, graveMaterial)
    grave.position.set(x, 0.1 + Math.random() * 0.4, z)
    grave.rotation.y = (Math.random() - 0.5) * 0.4;
    grave.rotation.z = (Math.random() - 0.5) * 0.3;
    grave.castShadow = true;
    graves.add(grave)
}

// Floor
const floor = new three.Mesh(
    new three.PlaneBufferGeometry(20, 20),
    new three.MeshStandardMaterial({
        map: grassColorTexture,
        aoMap: grassAmbientTexture,
        normalMap: grassNormalTexture,
        roughnessMap: grassRoughnessTexture,
    })
);
floor.geometry.setAttribute('uv2', new three.Float32BufferAttribute(floor.geometry.attributes.uv.array, 2))
floor.rotation.x = -Math.PI * 0.5;
floor.position.y = 0;
floor.receiveShadow = true;
scene.add(floor);

/**
 * Lights
 */
// Ambient light
const ambientLight = new three.AmbientLight("#b9d5ff", 0.12);
gui.add(ambientLight, "intensity").min(0).max(1).step(0.001);
scene.add(ambientLight);

// Directional light
const moonLight = new three.DirectionalLight("#b9d5ff", 0.12);
moonLight.position.set(4, 5, -2);
gui.add(moonLight, "intensity").min(0).max(1).step(0.001);
gui.add(moonLight.position, "x").min(-5).max(5).step(0.001);
gui.add(moonLight.position, "y").min(-5).max(5).step(0.001);
gui.add(moonLight.position, "z").min(-5).max(5).step(0.001);
scene.add(moonLight);

// Door Light
const doorLight = new three.PointLight('#ff7d46', 1, 7)
doorLight.position.set(0, 2.2, 2.7)
house.add(doorLight)

/**
 * Ghosts
 */

const ghost1 = new three.PointLight('#ff00ff', 2, 3)
const ghost2 = new three.PointLight('#00ffff', 2, 3)
const ghost3 = new three.PointLight('#ffff00', 2, 3)

scene.add(ghost1, ghost2, ghost3)
/**
 * Sizes
 */
const sizes = {
    width: window.innerWidth,
    height: window.innerHeight,
};

window.addEventListener("resize", () => {
    // Update sizes
    sizes.width = window.innerWidth;
    sizes.height = window.innerHeight;

    // Update camera
    camera.aspect = sizes.width / sizes.height;
    camera.updateProjectionMatrix();

    // Update renderer
    renderer.setSize(sizes.width, sizes.height);
    renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2));
});

/**
 * Camera
 */
// Base camera
const camera = new three.PerspectiveCamera(
    75,
    sizes.width / sizes.height,
    0.1,
    100
);
camera.position.x = 4;
camera.position.y = 2;
camera.position.z = 5;
scene.add(camera);

// Controls
const controls = new OrbitControls(camera, canvas);
controls.enableDamping = true;

/**
 * Renderer
 */
const renderer = new three.WebGLRenderer({
    canvas: canvas,
});
renderer.setSize(sizes.width, sizes.height);
renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2));
renderer.setClearColor('#262836')
/**
 * Shadows
 */
renderer.shadowMap.enabled = true;
renderer.shadowMap.type = three.PCFSoftShadowMap

moonLight.castShadow = true;
moonLight.shadow.mapSize.width = 256
moonLight.shadow.mapSize.height = 256
moonLight.shadow.camera.far = 7

doorLight.castShadow = true;
doorLight.shadow.mapSize.width = 256
doorLight.shadow.mapSize.height = 256
doorLight.shadow.camera.far = 7

ghost1.castShadow = true
ghost1.shadow.mapSize.width = 256
ghost1.shadow.mapSize.height = 256
ghost1.shadow.camera.far = 7

ghost2.castShadow = true
ghost2.shadow.mapSize.width = 256
ghost2.shadow.mapSize.height = 256
ghost2.shadow.camera.far = 7

ghost3.castShadow = true
ghost3.shadow.mapSize.width = 256
ghost3.shadow.mapSize.height = 256
ghost3.shadow.camera.far = 7

walls.castShadow = true
bushOne.castShadow = true
bushTwo.castShadow = true
bushThree.castShadow = true
bushFour.castShadow = true

/**
 * Animate
 */
const clock = new three.Clock();

const tick = () => {
    const elapsedTime = clock.getElapsedTime();

    // Update controls
    controls.update();

    // Update ghosts
    const ghost1Angle = elapsedTime * 0.5;
    ghost1.position.x = Math.cos(ghost1Angle) * 4;
    ghost1.position.z = Math.sin(ghost1Angle) * 4;
    ghost1.position.y = Math.sin(elapsedTime * 3);

    const ghost2Angle = - elapsedTime * 0.32;
    ghost2.position.x = Math.cos(ghost2Angle) * 4;
    ghost2.position.z = Math.sin(ghost2Angle) * 4;
    ghost2.position.y = Math.sin(elapsedTime * 3) + Math.sin(elapsedTime * 2.5);

    const ghost3Angle = - elapsedTime * 0.18;
    ghost3.position.x = Math.cos(ghost3Angle) * (7 + Math.sin(elapsedTime * 0.32));
    ghost3.position.z = Math.sin(ghost3Angle) * (7 + Math.sin(elapsedTime * 0.5));
    ghost3.position.y = Math.sin(elapsedTime * 3) + Math.sin(elapsedTime * 2);

    // Render
    renderer.render(scene, camera);

    // Call tick again on the next frame
    window.requestAnimationFrame(tick);
};

tick();
