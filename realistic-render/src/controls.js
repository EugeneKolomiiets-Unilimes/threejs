import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js'
import camera from './camera'
/**
 * Canvas
 */
const canvas = document.querySelector('canvas.webgl')

/**
 * Controls
 */
const controls = new OrbitControls(camera, canvas)
controls.enableDamping = true

export default controls